﻿namespace PrinterRegistrasi
{
    partial class FrmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtKartu = new System.Windows.Forms.TextBox();
            this.txtLabel = new System.Windows.Forms.TextBox();
            this.txtFormulir = new System.Windows.Forms.TextBox();
            this.txtTracer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAdmission = new System.Windows.Forms.TextBox();
            this.txtIdentitas = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtSep = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRujukan = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblGelang = new System.Windows.Forms.Label();
            this.txtGelangD = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGelangB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(164, 12);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(182, 20);
            this.txtPort.TabIndex = 1;
            // 
            // txtKartu
            // 
            this.txtKartu.Location = new System.Drawing.Point(164, 38);
            this.txtKartu.Name = "txtKartu";
            this.txtKartu.Size = new System.Drawing.Size(182, 20);
            this.txtKartu.TabIndex = 2;
            // 
            // txtLabel
            // 
            this.txtLabel.Location = new System.Drawing.Point(164, 65);
            this.txtLabel.Name = "txtLabel";
            this.txtLabel.Size = new System.Drawing.Size(182, 20);
            this.txtLabel.TabIndex = 3;
            // 
            // txtFormulir
            // 
            this.txtFormulir.Location = new System.Drawing.Point(164, 92);
            this.txtFormulir.Name = "txtFormulir";
            this.txtFormulir.Size = new System.Drawing.Size(182, 20);
            this.txtFormulir.TabIndex = 4;
            // 
            // txtTracer
            // 
            this.txtTracer.Location = new System.Drawing.Point(164, 119);
            this.txtTracer.Name = "txtTracer";
            this.txtTracer.Size = new System.Drawing.Size(182, 20);
            this.txtTracer.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Printer Kartu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Printer Label";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Printer Formulir";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Printer Tracer";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Printer Slip Admission";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Printer Identitas Pasien";
            // 
            // txtAdmission
            // 
            this.txtAdmission.Location = new System.Drawing.Point(164, 172);
            this.txtAdmission.Name = "txtAdmission";
            this.txtAdmission.Size = new System.Drawing.Size(182, 20);
            this.txtAdmission.TabIndex = 11;
            // 
            // txtIdentitas
            // 
            this.txtIdentitas.Location = new System.Drawing.Point(164, 145);
            this.txtIdentitas.Name = "txtIdentitas";
            this.txtIdentitas.Size = new System.Drawing.Size(182, 20);
            this.txtIdentitas.TabIndex = 10;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(164, 313);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtSep
            // 
            this.txtSep.Location = new System.Drawing.Point(164, 199);
            this.txtSep.Name = "txtSep";
            this.txtSep.Size = new System.Drawing.Size(182, 20);
            this.txtSep.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(78, 202);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Printer SEP";
            // 
            // txtRujukan
            // 
            this.txtRujukan.Location = new System.Drawing.Point(164, 226);
            this.txtRujukan.Name = "txtRujukan";
            this.txtRujukan.Size = new System.Drawing.Size(182, 20);
            this.txtRujukan.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(59, 229);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Printer Rujukan";
            // 
            // lblGelang
            // 
            this.lblGelang.AutoSize = true;
            this.lblGelang.Location = new System.Drawing.Point(59, 255);
            this.lblGelang.Name = "lblGelang";
            this.lblGelang.Size = new System.Drawing.Size(85, 13);
            this.lblGelang.TabIndex = 20;
            this.lblGelang.Text = "Printer Gelang D";
            // 
            // txtGelangD
            // 
            this.txtGelangD.Location = new System.Drawing.Point(164, 252);
            this.txtGelangD.Name = "txtGelangD";
            this.txtGelangD.Size = new System.Drawing.Size(182, 20);
            this.txtGelangD.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(59, 281);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Printer Gelang B";
            // 
            // txtGelangB
            // 
            this.txtGelangB.Location = new System.Drawing.Point(164, 278);
            this.txtGelangB.Name = "txtGelangB";
            this.txtGelangB.Size = new System.Drawing.Size(182, 20);
            this.txtGelangB.TabIndex = 21;
            // 
            // FrmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 351);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtGelangB);
            this.Controls.Add(this.lblGelang);
            this.Controls.Add(this.txtGelangD);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtRujukan);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtSep);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtAdmission);
            this.Controls.Add(this.txtIdentitas);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTracer);
            this.Controls.Add(this.txtFormulir);
            this.Controls.Add(this.txtLabel);
            this.Controls.Add(this.txtKartu);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.label1);
            this.Name = "FrmSetting";
            this.Text = "FrmSetting";
            this.Load += new System.EventHandler(this.FrmSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtKartu;
        private System.Windows.Forms.TextBox txtLabel;
        private System.Windows.Forms.TextBox txtFormulir;
        private System.Windows.Forms.TextBox txtTracer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAdmission;
        private System.Windows.Forms.TextBox txtIdentitas;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtSep;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRujukan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblGelang;
        private System.Windows.Forms.TextBox txtGelangD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtGelangB;
    }
}