﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrinterRegistrasi
{
    public partial class FrmSetting : Form
    {
        public FrmSetting()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings.Remove("PORT");
            config.AppSettings.Settings.Add("PORT", txtPort.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterKartu");
            config.AppSettings.Settings.Add("PrinterKartu", txtKartu.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterLabel");
            config.AppSettings.Settings.Add("PrinterLabel", txtLabel.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterFormulir");
            config.AppSettings.Settings.Add("PrinterFormulir", txtFormulir.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterTracer");
            config.AppSettings.Settings.Add("PrinterTracer", txtTracer.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterIdentitasPasien");
            config.AppSettings.Settings.Add("PrinterIdentitasPasien", txtIdentitas.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterSlipAdmission");
            config.AppSettings.Settings.Add("PrinterSlipAdmission", txtAdmission.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterSep");
            config.AppSettings.Settings.Add("PrinterSep", txtSep.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterRujukan");
            config.AppSettings.Settings.Add("PrinterRujukan", txtRujukan.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterGelangD");
            config.AppSettings.Settings.Add("PrinterGelangD", txtGelangD.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            config.AppSettings.Settings.Remove("PrinterGelangB");
            config.AppSettings.Settings.Add("PrinterGelangB", txtGelangB.Text);
            config.Save(ConfigurationSaveMode.Minimal);
            MessageBox.Show("Silahkan buka ulang aplikasi", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.Exit();
        }

        private void FrmSetting_Load(object sender, EventArgs e)
        {
            txtPort.Text = ConfigurationManager.AppSettings["PORT"];
            txtKartu.Text = ConfigurationManager.AppSettings["PrinterKartu"];
            txtLabel.Text = ConfigurationManager.AppSettings["PrinterLabel"];
            txtFormulir.Text = ConfigurationManager.AppSettings["PrinterFormulir"];
            txtTracer.Text = ConfigurationManager.AppSettings["PrinterTracer"];
            txtIdentitas.Text = ConfigurationManager.AppSettings["PrinterIdentitasPasien"];
            txtAdmission.Text = ConfigurationManager.AppSettings["PrinterSlipAdmission"];
            txtSep.Text = ConfigurationManager.AppSettings["PrinterSep"];
            txtRujukan.Text = ConfigurationManager.AppSettings["PrinterRujukan"];
            txtGelangD.Text = ConfigurationManager.AppSettings["PrinterGelangD"];
            txtGelangB.Text = ConfigurationManager.AppSettings["PrinterGelangB"];
        }
    }
}
