﻿using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Reporting.WinForms;
using PrinterRegistrasi.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

namespace PrinterRegistrasi
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        Listener listener;

        private void Listener_SocketAccepted(Socket e)
        {
            Client client = new Client(e);
            client.Received += Client_Received;
            client.Disconnected += Client_Disconnected;
        }


        private void Client_Disconnected(Client sender)
        {
            sender.Close();
        }

        private void Client_Received(Client sender, byte[] data)
        {
            //Invoke((MethodInvoker)delegate
            //{
            string message = Encoding.Default.GetString(data);

            string[] messageCommand = message.Split(' ');

            try
            {
                
                Invoke((MethodInvoker)delegate
                {
                    var c = dataGridView1.Rows.Count;
                    if (c >= 20)
                    {
                        c = c-1;
                        dataGridView1.Rows.RemoveAt(0);
                    }
                    dataGridView1.Rows.Add(message, DateTime.Now.ToString(), "");
                    dataGridView1.Rows[c].Tag = sender;
                });

                if (messageCommand.Length > 1)
                {
                    ExecutePrintCommand(messageCommand);
                }

                Invoke((MethodInvoker)delegate
                {
                    int i = dataGridView1.Rows.Count;
                    while (i > 0)
                    {
                        i--;
                        Client client = dataGridView1.Rows[i].Tag as Client;

                        if (client.ID == sender.ID)
                        {
                            dataGridView1.Rows[i].Cells[2].Value = "Success";
                            break;
                        }
                    }
                    Client_Disconnected(sender);
                });
            }
            catch (Exception ex)
            {
                Invoke((MethodInvoker)delegate
                {
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        Client client = dataGridView1.Rows[i].Tag as Client;

                        if (client.ID == sender.ID)
                        {
                            dataGridView1.Rows[i].Cells[2].Value = ex.Message;
                            break;
                        }
                    };
                });
            }
            //});
        }

        private void ExecutePrintCommand(string[] commandLine)
        {
            switch (commandLine[0])
            {
                case "Print_Kartu":
                    PrintKartu(commandLine[1]);
                    break;
                case "Print_Formulir":
                    PrintFormulir(commandLine[1]);
                    break;
                case "Print_Label":
                    PrintLabel(commandLine[1], short.Parse(commandLine[2]));
                    break;
                case "Print_Tracer":
                    PrintTracer(commandLine[1]);
                    break;
                case "Print_IdentitasPasien":
                    PrintIdentitasPasien(commandLine[1]);
                    break;
                case "Print_SlipAdmission":
                    PrintSlipAdmission(commandLine[1]);
                    break;
                case "Print_SEP":
                    PrintSEP(commandLine[1]);
                    break;
                case "Print_Rujukan":
                    PrintRujukan(commandLine[1]);
                    break;
                case "Print_GelangD":
                    PrintGelangD(commandLine[1], short.Parse(commandLine[2]));
                    break;
                case "Print_GelangB":
                    PrintGelangB(commandLine[1], short.Parse(commandLine[2]));
                    break;
            }
        }

        private void PrintKartu(string noreg)
        {
            // setting
            string nama_report = "KartuPasien";
            string nama_sp = "Print_KartuPasien";
            var service = new Service();
            var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg)
            });
            service = null;

            ReportDocument rdoc = new ReportDocument();
            rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
            rdoc.SetDataSource(ds.Tables[nama_sp]);
            rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterKartu"];
            rdoc.PrintToPrinter(1, false, 1, 1);
            rdoc.Close();
            rdoc = null;
        }

        private void PrintLabel(string noreg, short jumlah)
        {
            try
            {
                string nama_report = "LabelBarcode";
                string nama_sp = "RSUB_LoopDataLabel";
                var service = new Service();
                var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg,@jml,@jml2 ", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg),
                new ObjParameter("jml", jumlah),
                new ObjParameter("jml2", 0)
            });

                service = null;
                ReportDocument rdoc = new ReportDocument();
                rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
                rdoc.SetDataSource(ds.Tables[nama_sp]);
                rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterLabel"];
                rdoc.PrintToPrinter(1, false, 1, 1);
                rdoc.Close();
                rdoc = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void PrintFormulir(string noreg)
        {
            string nama_report = "CetakFormulir";
            string nama_sp = "PrintAntrian";
            var service = new Service();

            var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg)
            });
            service = null;

            ReportDocument rdoc = new ReportDocument();
            rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
            rdoc.SetDataSource(ds.Tables[nama_sp]);
            rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterFormulir"];
            rdoc.PrintToPrinter(1, false, 1, 1);
            rdoc.Close();
            rdoc = null;
        }

        private void PrintTracer(string noreg)
        {
            string nama_report = "CetakTracer";
            string nama_sp = "PrintTracer";
            var service = new Service();

            var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg)
            });
            service = null;

            ReportDocument rdoc = new ReportDocument();
            rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
            rdoc.SetDataSource(ds.Tables[nama_sp]);
            rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterTracer"];
            rdoc.PrintToPrinter(1, false, 1, 1);
            rdoc.Close();
            rdoc = null;
        }

        private void PrintIdentitasPasien(string noreg)
        {
            try
            {
                string nama_report = "LembarIdentitasPasien";
                string nama_sp = "RSBM_LembarIdentitasPasien";
                var service = new Service();

                var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
                {
                    new ObjParameter("NoReg", noreg)
                });
                service = null;

                ReportDocument rdoc = new ReportDocument();
                rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
                rdoc.SetDataSource(ds.Tables[nama_sp]);
                rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterIdentitasPasien"];
                rdoc.PrintToPrinter(1, false, 1, 1);
                rdoc.Close();
                rdoc = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void PrintSlipAdmission(string noreg)
        {
            string nama_report = "CetakSlipAdmission";
            string nama_sp = "RSBM_SlipAdmission";
            var service = new Service();

            var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg)
            });
            service = null;

            ReportDocument rdoc = new ReportDocument();
            rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
            rdoc.SetDataSource(ds.Tables[nama_sp]);
            rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterSlipAdmission"];
            rdoc.PrintToPrinter(1, false, 1, 1);
            rdoc.Close();
            rdoc = null;
        }

        private void PrintSEP(string nosep)
        {
            string nama_report = "CetakSEP";
            string nama_sp = "Z_BPJStrSEPVClaim";
            var service = new Service();

            var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoBukti", new List<ObjParameter>()
            {
                new ObjParameter("NoBukti", nosep)
            });
            service = null;

            ReportDocument rdoc = new ReportDocument();
            rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
            rdoc.SetDataSource(ds.Tables[nama_sp]);
            rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterSep"];
            rdoc.PrintToPrinter(1, false, 1, 1);
            rdoc.Close();
            rdoc = null;
        }

        private void PrintRujukan(string nobukti)
        {
            string nama_report = "CetakRujukan";
            string nama_sp = "Z_BPJStrRujukanVClaim";
            var service = new Service();

            var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoBukti", new List<ObjParameter>()
            {
                new ObjParameter("NoBukti", nobukti)
            });
            service = null;

            ReportDocument rdoc = new ReportDocument();
            rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
            rdoc.SetDataSource(ds.Tables[nama_sp]);
            rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterRujukan"];
            rdoc.PrintToPrinter(1, false, 1, 1);
            rdoc.Close();
            rdoc = null;
        }

        private void PrintGelangD(string noreg, short jumlah)
        {
            try
            {
                string nama_report = "GelangPasien";
                string nama_sp = "CetakGelang";
                var service = new Service();
                var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg)
            });

                service = null;
                ReportDocument rdoc = new ReportDocument();
                rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
                rdoc.SetDataSource(ds.Tables[nama_sp]);
                rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterGelangD"];
                rdoc.PrintToPrinter(jumlah, false, 1, 1);
                rdoc.Close();
                rdoc = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void PrintGelangB(string noreg, short jumlah)
        {
            try
            {
                string nama_report = "GelangPasien";
                string nama_sp = "CetakGelang";
                var service = new Service();
                var ds = service.Execute(nama_sp, $"EXEC {nama_sp} @NoReg", new List<ObjParameter>()
            {
                new ObjParameter("NoReg", noreg)
            });

                service = null;
                ReportDocument rdoc = new ReportDocument();
                rdoc.Load(Path.Combine(Application.StartupPath, $"Report\\{nama_report}.rpt"));
                rdoc.SetDataSource(ds.Tables[nama_sp]);
                rdoc.PrintOptions.PrinterName = ConfigurationManager.AppSettings["PrinterGelangB"];
                rdoc.PrintToPrinter(jumlah, false, 1, 1);
                rdoc.Close();
                rdoc = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        private void btnSetting_Click(object sender, EventArgs e)
        {
            var f = new FrmSetting();
            f.ShowDialog();
            lblPort.Text = ConfigurationManager.AppSettings["PORT"];
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Maximized;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            var p = Process.GetProcessesByName("PrinterRegistrasi");
            if (p.Count() > 1)
            {
                MessageBox.Show("Application is open ready", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            lblPort.Text = ConfigurationManager.AppSettings["PORT"];
            listener = new Listener(int.Parse(ConfigurationManager.AppSettings["PORT"] == null ? "3024" : ConfigurationManager.AppSettings["PORT"]));
            listener.SocketAccepted += Listener_SocketAccepted;
            listener.Start();
            notifyIcon1.Visible = true;
            notifyIcon1.Icon = new Icon(this.Icon, 40, 40);
            notifyIcon1.BalloonTipTitle = "Printer Registrasi";
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            //notifyIcon1.ShowBalloonTip(1000);
            this.Hide();
        }

        private void Main_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                btnHide.PerformClick();
            }
        }
    }
}
