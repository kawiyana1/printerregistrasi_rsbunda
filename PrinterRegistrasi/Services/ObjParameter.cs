﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterRegistrasi.Services
{
    public class ObjParameter
    {
        public string Name { get; set; }
        public object Value { get; set; }

        public ObjParameter(string name, object value)
        {
            Name = name;
            Value = value;
        }
    }
}
