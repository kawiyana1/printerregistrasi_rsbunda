﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinterRegistrasi.DataSet;
using System.Configuration;
using PrinterRegistrasi.Services;

namespace PrinterRegistrasi.Services
{
    public class Service
    {
        public SIMDataSet Execute(string sp, string query, List<ObjParameter> param)
        {
            string conString = ConfigurationManager.ConnectionStrings["SIMConnectionString"].ConnectionString;
            SqlCommand cmd = new SqlCommand(query);
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    foreach (var x in param)
                    {
                        cmd.Parameters.AddWithValue($"@{x.Name}", x.Value);
                    }
                    sda.SelectCommand = cmd;
                    using (var ds = new SIMDataSet())
                    {
                        ds.EnforceConstraints = false;
                        sda.Fill(ds, sp);
                        return ds;
                    }
                }
            }
        }
    }
}
